FLOWS EMITTER
=============
Simulating the Netflow flows emitter (which is usually done by a router)

- *Input*: the Netflow v4 datasets

- *Output*: Netflow flows v4 to be captured at a specific collector machine on LAN

TODO
====
Send flow data to IP:port using UDP protocol

Compiling Instruction
===================

Make sure you have boost C++ library installed

clang++/g++ FlowEmitter.cpp -o your_execuatable -lboost_system -lboost_thread-mt

./your_executable /path/to/Dataset.txt
