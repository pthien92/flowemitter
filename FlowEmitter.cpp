#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <sstream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <ctime>
#include <boost/thread/thread.hpp>
using namespace std;

struct CustomRow {
  string _srcIP, _dstIP, _proto, _src_port, _dst_port,
         _packets, _bytes, _start_time, _end_time, _duration, _avg_pkt_size,
         _type_of_service, _urg_flag, _ack_flag, _push_flag, _rst_flag, _syn_flag,
         _fin_flag, _class_type;
  CustomRow(string srcIP, string dstIP, string proto, string src_port,
      string dst_port, string packets, string bytes, string start_time,
      string end_time, string duration, string avg_pkt_size, 
      string type_of_service, string urg_flag, string ack_flag, string push_flag,
      string rst_flag, string syn_flag, string fin_flag, string class_type) {
    _srcIP          = srcIP;
    _dstIP          = dstIP;
    _proto          = proto;
    _src_port       = src_port;
    _dst_port       = dst_port;
    _packets        = packets;
    _bytes          = bytes;
    _start_time     = start_time;
    _end_time       = end_time;
    _duration       = duration;
    _avg_pkt_size   = avg_pkt_size;
    _type_of_service = type_of_service;
    _urg_flag       = urg_flag;
    _ack_flag       = ack_flag;
    _push_flag      = push_flag;
    _rst_flag       = rst_flag;
    _syn_flag       = syn_flag;
    _fin_flag       = fin_flag;
    _class_type     = class_type;

  }
};
boost::mutex _mutex;
void emitFlow(vector<CustomRow*>& data, unsigned int* currentIndex, double count) {
  _mutex.lock();
  while (stod(data[*currentIndex]->_start_time) < count) {
    // TODO: sent flow data via UDP/IP connection to LAN address
    cout << "sent" << endl;
    ++(*currentIndex);
  }
  _mutex.unlock();
}

void increaseCounter(const boost::system::error_code& /*e*/,
    boost::asio::deadline_timer* t, double* count, double MAX, vector<CustomRow*>& data, unsigned int* index) {
  if (*count < MAX) {
    ++(*count);
    time_t local_time_unix = *count;
    cout << ctime(&local_time_unix) << "\n";
    boost::thread* thr = new boost::thread(emitFlow, boost::ref(data), index, *count);
    thr->detach();
    t->expires_at(t->expires_at() + boost::posix_time::seconds(1));
    t->async_wait(boost::bind(increaseCounter,
          boost::asio::placeholders::error, t, count, MAX, data, index));
  } 
}



int main(int argc, char *argv[])
{
  ifstream inFile;
  string filename = string(argv[1]);
  inFile.open(filename);
  string sLine;
  getline(inFile,sLine); //dump the label;
  vector<CustomRow*> data;
  cout << "here" << endl;
  while (getline(inFile, sLine)) {
    if (sLine.empty())
      break;
    vector<string> row;
    string sWord;
    stringstream stream(sLine);
    while (getline(stream, sWord, ',')) {
      row.push_back(sWord);
    }
    data.push_back(new CustomRow(    row[0],  row[1],  row[2],  row[3],  row[4],
          row[5],  row[6],  row[7],  row[8],  row[9],  row[10], row[11], row[12],
          row[13], row[14], row[15], row[16], row[17], row[18]
          ));
    
  }
  
  sort(data.begin(), data.end(), [](const CustomRow* left, const CustomRow* right) {
      return stod(left->_start_time) < stod(right->_start_time);
      });
  double count = stod(data[0]->_start_time);
  unsigned int i = 0;
  unsigned int FLOWS_NUMBER = data.size();
  double MAX_COUNT = stod(data.back()->_start_time);
  boost::asio::io_service io;
  boost::asio::deadline_timer t(io, boost::posix_time::seconds(1));
  t.async_wait(boost::bind(increaseCounter,
        boost::asio::placeholders::error, &t, &count, MAX_COUNT, data, &i));
  io.run();
  return 0;
}
